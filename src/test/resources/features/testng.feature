Feature: Login feature for the given website

  Scenario: when the user enters the login credentials the home page must be displayed
    Given Open the website
    Then The home page of the website get displayed
    When Click on the login profile button
    And Enter the email in the email field
    And Enter the password in the password field
    And click on the login button
    And The home page with username should be displayed
    And click the logout button
    Then cthe user should taken to the homepage
    And close the browser
